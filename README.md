# README #

### What is this repository for? ###

* Simple example of using a Mule custom validator
* Version 1.0.0
* This will take a CSV file, as three field records
		Name,ID,Value
  The file will be trasformed via DataWeave into a list of Java Class with three members, 
  		name (String)
		ID (Sting)
		value (Float)
  The list will then be validate for the following rules:
  		name 	cannot be null, 
				must be at least 5 chars
				must not be more than 45 chars
		ID 		cannot be null
				must be at least one digit
					must be no more than 14 digits
					OR
					must be no more than 12 digits followes by "_A"
		value	cannot be null (This can actually never occur, DW will fail on an invalid number)
				must be at least 10.0
				must be less than 1000.0
  During validation, any failed field will be reported with the failure reason.
  After validation, each failed record will be reported which can be matched to the fail reason earlier reported.
  After validation, each valid record will be processed to indicate if it meets the "_A" format or not.
  		

### Who do I talk to? ###

* Dennis, but he will probably be of no help.